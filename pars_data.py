#!/usr/bin/python

import numpy as np
import mvasup
import specsup
import math
import tfasistant
import sys
from array import array

prefix = '_4D_HFcorr'
file_prefix = '_2b'#'_njnb_blind'#'_2b'#'_7jin2bex-7jex3bin0J'#
prefix += file_prefix
#input
filename = '/eos/user/a/asonay/MVA_Data/data/ttbar_PhPy8_AFII_AllFilt_HFcorr'+file_prefix+'.csv'
filename_data = '/eos/user/a/asonay/MVA_Data/data/data'+file_prefix+'.csv'
filename_nonttbar = '/eos/user/a/asonay/MVA_Data/data/non-ttbar'+file_prefix+'.csv'


#Selection of variables to be trained.
#variable_names = ['Alt$(jet_pcb_MV2c10_btag_ordered[2],0)','nJets','Alt$(jet_pcb_MV2c10_btag_ordered[3],0)','Alt$(jet_pcb_MV2c10_btag_ordered[4],0)']
variable_names = ['nRCJetsM100','nJets','jet_pt[0]','jet_pt[1]','jet_pt[2]','jet_pt[3]','jet_pt[4]','jet_pt[5]','jet_pt[6]','Alt$(jet_pt[7],0)','Alt$(jet_pt[8],0)','Sum$(jet_pt*(Iteration$>=9))','Alt$(el_pt[0],0)+Alt$(mu_pt[0],0)','met_met']

print ("\nReading file..")
try:
    f = open(filename)
except IOError:
    print(filename,'not accessible')
    sys.exit()
header = f.readline()
header_names = np.array(header.split(';'))
selected_variable,number_of_variable = mvasup.SelectVariables(variable_names,header_names)
dataset = np.loadtxt(filename, delimiter=';', skiprows=1, converters={number_of_variable+2: lambda x:int(x=='s'.encode('utf-8')) })
dataset_data = np.loadtxt(filename_data, delimiter=';', skiprows=1, converters={number_of_variable+2: lambda x:int(x=='s'.encode('utf-8')) })
dataset_nonttbar = np.loadtxt(filename_nonttbar, delimiter=';', skiprows=1, converters={number_of_variable+2: lambda x:int(x=='s'.encode('utf-8')) })
#neg_w = [i for i in range(len(dataset)) if dataset[i][16]<0]
print ("Completed..\n")


variable_size = len(selected_variable)
print ('    \nVariable Size : ',variable_size)

X_data = dataset_data[:,selected_variable]
X_nonttbar = dataset_nonttbar[:,selected_variable]
X_mc = dataset[:,selected_variable]
data_size,nonttbar_size,mc_size = len(X_data),len(X_nonttbar),len(X_mc)
W_data = np.ones(data_size)
W_nonttbar = dataset_nonttbar[:,number_of_variable+1] * -1
W_mc = dataset[:,number_of_variable+1]

Wnmc = sum(W_mc)
Wnnonttbar = sum(W_nonttbar)

print ('Data Size:',data_size,'Non-ttbar Size:',nonttbar_size,'MC Size:',mc_size,'Weighted MC:',Wnmc,'Weighted non-ttbar:',Wnnonttbar)


Y_data,Y_nonttbar,Y_mc = np.ones(data_size), np.ones(nonttbar_size), np.zeros(mc_size)

X_data,Y_data,W_data=specsup.NjetSequence(X_data,Y_data,W_data,1)
X_nonttbar,Y_nonttbar,W_nonttbar=specsup.NjetSequence(X_nonttbar,Y_nonttbar,W_nonttbar,1)
X_mc,Y_mc,W_mc=specsup.NjetSequence(X_mc,Y_mc,W_mc,1)

X_ttbardata,Y_ttbardata,W_ttbardata = mvasup.Shuffle(X_data,X_nonttbar,Y_data,Y_nonttbar,W_data,W_nonttbar)
X,Y,W = mvasup.Shuffle(X_mc,X_ttbardata,Y_mc,Y_ttbardata,W_mc,W_ttbardata)
#Y=Y.reshape((len(Y), 1))
#W=W.reshape((len(W), 1))

lst_train,lst_test=mvasup.train_test_split(Y,50)

print ("Normalizing Inputs----------------------------------------")
norm=mvasup.Norm('strd_rw'+prefix,'norm/')
X_train=norm.Standardization(np.copy(X),variable_names)
print ("End of Normalization--------------------------------------\n")

Xno_train,Xo_train,Yo_train,Wo_train=X[lst_train,:],X_train[lst_train,:],Y[lst_train],W[lst_train]
Xno_test,Xo_test,Yo_test,Wo_test=X[lst_test,:],X_train[lst_test,:],Y[lst_test],W[lst_test]

np.savetxt('/eos/user/a/asonay/MVA_Data/data/traindata'+prefix+'.csv',np.c_[Xno_train,Xo_train,Yo_train,Wo_train])
np.savetxt('/eos/user/a/asonay/MVA_Data/data/testdata'+prefix+'.csv',np.c_[Xno_test,Xo_test,Yo_test,Wo_test])

