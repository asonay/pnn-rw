import os

f = open("../arguments.txt", "r")
line = f.readline()

hp_order=['hl','nd','lr','ep','bt']
hp_order_std=['nhl','nnode','lrate','nepoch','nbatch']

while line:
    line=line.rstrip()
    hp_arg=''
    hp_arg_std=''
    args=line.split(' ')
    for i in range(len(args)):
        hp_arg+=hp_order[i]+args[i]
        hp_arg_std+='_'+hp_order_std[i]+args[i]
    download_cmd1='rucio download user.asonay.hbsm.NNRW.'+hp_arg+'_output.txt'
    download_cmd2='rucio download user.asonay.hbsm.NNRW.'+hp_arg+'_out_plot_test_epoch99.png'
    download_cmd3='rucio download user.asonay.hbsm.NNRW.'+hp_arg+'_out_plot_train_epoch99.png'
    download_cmd4='rucio download user.asonay.hbsm.NNRW.'+hp_arg+'_out_model.h5'
    download_cmd5='rucio download user.asonay.hbsm.NNRW.'+hp_arg+'_out_weight.h5'
    mv_cmd1='mv user.asonay.hbsm.NNRW.'+hp_arg+'_output.txt/* /eos/user/a/asonay/MVA_Data/hpo/RW_output_1l_4D_HFcorr_2b'+hp_arg_std+'.txt'
    mv_cmd2='mv user.asonay.hbsm.NNRW.'+hp_arg+'_out_plot_test_epoch99.png/* /eos/user/a/asonay/MVA_Data/hpo_plot/goodnessofProb__4D_HFcorr_2b'+hp_arg_std+'test_epoch99.png'
    mv_cmd3='mv user.asonay.hbsm.NNRW.'+hp_arg+'_out_plot_train_epoch99.png/* /eos/user/a/asonay/MVA_Data/hpo_plot/goodnessofProb__4D_HFcorr_2b'+hp_arg_std+'train_epoch99.png'
    mv_cmd4='mv user.asonay.hbsm.NNRW.'+hp_arg+'_out_model.h5/* /eos/user/a/asonay/MVA_Data/weights/RW_model_1l_4D_HFcorr_2b'+hp_arg_std+'.h5'
    mv_cmd5='mv user.asonay.hbsm.NNRW.'+hp_arg+'_out_weight.h5/* /eos/user/a/asonay/MVA_Data/weights/RW_weight_1l_4D_HFcorr_2b'+hp_arg_std+'.h5'
    rm_cmd='rm -rf user.asonay*'
    os.system(download_cmd1)
    os.system(download_cmd2)
    os.system(download_cmd3)
    os.system(download_cmd4)
    os.system(download_cmd5)
    os.system(mv_cmd1)
    os.system(mv_cmd2)
    os.system(mv_cmd3)
    os.system(mv_cmd4)
    os.system(mv_cmd5)
    os.system(rm_cmd)
    line = f.readline()
