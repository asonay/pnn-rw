#!/usr/bin/env python

import numpy as np
from keras.models import Sequential
from keras.optimizers import SGD,Adam,RMSprop
from keras.layers import Dense, Dropout, LeakyReLU, AlphaDropout, BatchNormalization
import mvasup
import math
import tfasistant
import sys
from ROOT import TFile, TTree, TGraph, TCanvas
from array import array
from tensorflow.python.keras.callbacks import Callback
from keras.callbacks import LearningRateScheduler
from keras import backend as K
import tensorflow as tf

from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())
print("GPU Available: ", tf.test.is_gpu_available())

prefix = '_4D_HFcorr'
file_prefix = '_2b'#'_njnb_blind'#'_2b'#'_7jin2bex-7jex3bin0J'#
prefix += file_prefix

if len(sys.argv)!=7:
    print ('Usage:')
    print ('0- - <input_train,input_test>')
    print ('1- # hidden layer')
    print ('2- # nodes')
    print ('3- Learning rate')
    print ('4- epoch')
    print ('5- mini-batch size')
    sys.exit()

inputFile = sys.argv[1]
nhl = int(sys.argv[2])
nnode = int(sys.argv[3])
lrate = float(sys.argv[4])
nepoch = int(sys.argv[5])
nbatch = int(sys.argv[6])

print('INPUT FILE :',inputFile)

filename='training_data.csv'
TFile.Cp(inputFile,filename)

d = np.loadtxt(filename, delimiter=' ')

d1 = np.array([x[:d.shape[1]-1] for x in d if x[d.shape[1]-1]==1])
d2 = np.array([x[:d.shape[1]-1] for x in d if x[d.shape[1]-1]==0])

print (d1)
print (d2)

X1,X1_train,Y1,W1 = d1[:,0:14],d1[:,14:28],d1[:,28],d1[:,29]
X2,X2_train,Y2,W2 = d2[:,0:14],d2[:,14:28],d2[:,28],d2[:,29]

variable_size = X1.shape[1]
print (variable_size)

print (X1,Y1,W1)
print (X2,Y2,W2)

print ('########################')
print ('PROCESS STARTING FOR : ')
print ('#HL           : ',nhl)
print ('#NODE         : ',nnode)
print ('#EPOCH        : ',nepoch)
print ('#BATCH        : ',nbatch)
print ('Learning Rate : ',lrate)
print ('########################')

oprefix = prefix+'_nhl'+str(nhl)+'_nnode'+str(nnode)+'_lrate'+str(lrate)+'_nepoch'+str(nepoch)+'_nbatch'+str(nbatch)
#output
output_model_name = 'out_model.h5'
output_weights_name = 'out_weight.h5'
output_file_name = 'output.txt'

model = Sequential()
model.add(Dense(nnode, input_dim=variable_size, activation='relu', kernel_initializer='he_uniform'))
for i in range(nhl):
    model.add(Dense(nnode, activation='relu', kernel_initializer='he_uniform'))
model.add(Dense(1, activation='sigmoid'))
# compile the keras model
opt = SGD(lr=lrate, momentum=0.9)
model.compile(loss='mean_squared_error', optimizer=opt, metrics=['accuracy'])
print (model.summary())
# fit the keras model on the dataset
#Metric need to be optimized
nstep=nepoch
callback_train = tfasistant.Callbacks('train',X1_train,X1,Y1,W1,nstep,'out_plot_')
callback_test = tfasistant.Callbacks('test',X2_train,X2,Y2,W2,nstep,'out_plot_')
model.fit(X1_train, Y1, sample_weight = W1, epochs=nepoch, batch_size=nbatch, callbacks=[callback_train,callback_test])
# evaluate the keras model
model.save(output_model_name)
model.save_weights(output_weights_name)
print ("Training completed and storing process started..--------")
file_output = open(output_file_name, "w")
file_output.write('Loss Train All')
mvasup.WriteToFile(callback_train.GetLoss()[0],file_output)
file_output.write('Loss Train ge10j')
mvasup.WriteToFile(callback_train.GetLoss()[1],file_output)
file_output.write('Default Chi2 Train')
mvasup.WriteToFile(callback_train.GetChi2(),file_output)
file_output.write('Self Chi2 const Train ')
mvasup.WriteToFile(callback_train.GetChi2_self()[0],file_output)
file_output.write('Self Chi2 ge10j Train ')
mvasup.WriteToFile(callback_train.GetChi2_self()[1],file_output)
file_output.write('Self Chi2 se10j Train ')
mvasup.WriteToFile(callback_train.GetChi2_self()[2],file_output)
file_output.write('Loss Test All')
mvasup.WriteToFile(callback_test.GetLoss()[0],file_output)
file_output.write('Loss Test ge10j')
mvasup.WriteToFile(callback_test.GetLoss()[1],file_output)
file_output.write('Default Chi2 Test')
mvasup.WriteToFile(callback_test.GetChi2(),file_output)
file_output.write('Self Chi2 const Test ')
mvasup.WriteToFile(callback_test.GetChi2_self()[0],file_output)
file_output.write('Self Chi2 ge10j Test ')
mvasup.WriteToFile(callback_test.GetChi2_self()[1],file_output)
file_output.write('Self Chi2 se10j Test ')
mvasup.WriteToFile(callback_test.GetChi2_self()[2],file_output)
file_output.close()
print ("Storing the data has been done..------------------------")
K.clear_session()
    
