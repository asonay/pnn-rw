from tensorflow import ones_like, equal, log
from keras import backend as K
from tensorflow.python import mul
import tensorflow as tf
import math
import numpy as np
from tensorflow.python.keras.callbacks import Callback
import AnaStat

def weighted_categorical_crossentropy(weights):
    """
    A weighted version of keras.objectives.categorical_crossentropy
    
    Variables:
        weights: numpy array of shape (C,) where C is the number of classes
    
    Usage:
        weights = np.array([0.5,2,10]) # Class one at 0.5, class 2 twice the normal weights, class 3 10x.
        loss = weighted_categorical_crossentropy(weights)
        model.compile(loss=loss,optimizer='adam')
    """
    
    weights = K.variable(weights)
        
    def loss(y_true, y_pred):
        # scale predictions so that the class probas of each sample sum to 1
        y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
        # clip to prevent NaN's and Inf's
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        # calc
        loss = y_true * K.log(y_pred) * weights
        loss = -K.sum(loss, -1)
        return loss
    
    return loss

def weighted_binary_crossentropy(w1, w2):
  '''
  w1 and w2 are the weights for the two classes.
  Computes weighted binary crossentropy
  Use like so:  model.compile(loss=weighted_binary_crossentropy(), optimizer="adam", metrics=["accuracy"])
  '''

  def loss(y_true, y_pred):
      # avoid absolute 0
      y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
      ones = ones_like(y_true)
      msk = equal(y_true, ones)
      # tensor of booleans of length == y_true; true means that the true class is 1

      res, _ = tf.map_fn(lambda x: (mul(-log(x[0]), w1) if x[1] is True
                                    else mul(-log(1 - x[0]), w2), x[1]),
                         (y_pred, msk), dtype=(tf.float32, tf.bool))

      return res

  return loss



def diagnostic_p(sample_weight):
    weights = K.variable(sample_weight)
    def metric(true_labels, predictions):

        # TF Calculation has to be understood
        """
        y_true = K.variable(true_labels)
        y_pred = K.variable(predictions)
        print(y_true,y_pred,weights)

        y_true_np = K.eval(y_true)
        wd = tf.cast(100 * y_true * weights,tf.int32)
        wmc = tf.cast(100 * (1-y_true) * weights,tf.int32)

        predictions_int = tf.cast(y_pred*100,tf.int32)

        
        print(wd.dtype,wmc.dtype,predictions_int.dtype)
        
        pd = tf.math.bincount(predictions_int,weights=wd)
        pmc = tf.math.bincount(predictions_int,weights=wmc)

        ptot = pd+pmc
        
        p = pd/ptot
        x = tf.convert_to_tensor(np.array([0+i*(1./100.) for i in range(100)]))

        chi2 = tf.sum(((p-x)*(p-x))/x)/100.
        """
        return tf.constant(0.0)
    return metric

           
def epoch_norm_decay(epoch):
    lrate=0.01
    epoch_drop=40
    decay_rate=lrate/((epoch_drop-epoch)*(epoch_drop-epoch)+1)
    return decay_rate

def step_decay(epoch):
	initial_lrate = 0.01
	drop = 0.6
	epochs_drop = 10.0
	lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
	return lrate
    
import ROOT
from os import path
if path.exists('rootlogon.C'): ROOT.gROOT.Macro('rootlogon.C')

class Callbacks(Callback):
    def __init__(self, name, x, xor, y, w, n, location='plots/goodnessofProb_',prefix=''):
        self.name = name
        self.x = x
        self.xor = xor
        self.w = w
        self.y = y if (y.ndim == 1 or y.shape[1] == 1) else np.argmax(y, axis=1)
        self.n = n
        self.chi2=0
        self.chi2_list=[]
        self.chi2_self=0
        self.chi2_self_list=[]
        self.chi2_ge10j_self=0
        self.chi2_ge10j_self_list=[]
        self.chi2_se10j_self=0
        self.chi2_se10j_self_list=[]
        self.loss_ge10j_self=0
        self.loss_ge10j_self_list=[]
        self.loss_self=0
        self.loss_self_list=[]
        self.hist_list=[]
        self.lrate=[]
        self.ha=ROOT.TH1D('ha_'+self.name+prefix,'',100,0.,1.)
        self.hb=ROOT.TH1D('hb_'+self.name+prefix,'',100,0.,1.)
        self.hage10j=ROOT.TH1D('hage10j_'+self.name+prefix,'',100,0.,1.)
        self.hbge10j=ROOT.TH1D('hbge10j_'+self.name+prefix,'',100,0.,1.)
        self.hase10j=ROOT.TH1D('hase10j_'+self.name+prefix,'',100,0.,1.)
        self.hbse10j=ROOT.TH1D('hbse10j_'+self.name+prefix,'',100,0.,1.)
        self.hc=ROOT.TH1D('hc_'+self.name+prefix,'',100,0.,1.)
        self.loc=location+prefix

    def reset_hist(self):
        self.ha.Reset("ICESM")
        self.hage10j.Reset("ICESM")
        self.hase10j.Reset("ICESM")
        self.hb.Reset("ICESM")
        self.hbge10j.Reset("ICESM")
        self.hbse10j.Reset("ICESM")
        self.hc.Reset("ICESM")
 
    def save_plot(self,epoch,save=True):
        if save == True:
            self.hb.SetLineWidth(4)
            self.hb.SetLineColor(1)
            self.hbge10j.SetLineWidth(4)
            self.hbge10j.SetLineColor(3)
            self.hbge10j.SetMarkerStyle(24)
            self.hbse10j.SetLineWidth(4)
            self.hbse10j.SetLineColor(6)
            self.hbse10j.SetMarkerStyle(24)
            self.hc.SetLineWidth(2)
            self.hc.SetLineColor(2)

            hcan = ROOT.TH2F('hcan_'+self.name+str(epoch),'',100,0,1.01,100,0,1.01)
            hcan.GetXaxis().SetTitle('NN Score')
            hcan.GetYaxis().SetTitle('P(data(t#bar{t})|#vec{x})')
           
            c = ROOT.TCanvas('c_'+self.name+str(epoch))
            c.SetTickx()
            c.SetTicky()
            hcan.Draw()
            self.hc.Draw('same hist L')
            self.hb.Draw('same e1')
            self.hbge10j.Draw('same e1')
            self.hbse10j.Draw('same e1')
            lt = ROOT.TLatex();
            lt.SetTextSize(0.028)
            lt.SetTextAlign(13)
            lt.DrawLatex(.1,.95,'#EPOCH: '+str(epoch))
            lt.DrawLatex(.1,.9,'SAMPLE: '+self.name)
            lt.DrawLatex(.1,.85,'#chi^{2}/ndf: '+str(self.chi2_self))
            c.SaveAs(self.loc+self.name+'_epoch'+str(epoch)+'.png')
            c.Close()
 
    def on_epoch_end(self, epoch, logs={}):
        if (epoch+1)%self.n==0:
            self.reset_hist()
            optimizer = self.model.optimizer
            _lr,_decay,_iter = optimizer.lr,optimizer.decay,optimizer.iterations
            _lr=tf.cast(_lr, dtype=tf.float32)
            _decay=tf.cast(_decay, dtype=tf.float32)
            _iter=tf.cast(_iter, dtype=tf.float32)
            lr = K.eval(_lr * (1. / (1. + _decay * _iter)))
            self.lrate.append(lr)
            print('Current learning rate :',lr)
            
            y_hat = np.asarray(self.model.predict(self.x))
        
            n=100
            xmin = 0.
            xmax = 1.
            dx = (xmax-xmin)/n
            pd = [0]*(n+1)
            pmc = [0]*(n+1)
            
            print ('Model Evaluated.. Next: Creating histograms..')
            count_loss=0
            for var,x,x_true,w in zip(self.xor,y_hat,self.y,self.w):
                self.loss_self+=(x_true-x)**2
                if var[1]>=10:
                    self.loss_ge10j_self+=(x_true-x)**2
                    count_loss+=1
                if x_true==1:
                    self.hb.Fill(x,w)
                    if var[1]>10 and var[0]<=1500000:
                        self.hbge10j.Fill(x,w)
                    elif var[1]<9 and var[0]<=1500000:
                        self.hbse10j.Fill(x,w)
                else:
                    self.ha.Fill(x,w)
                    if var[1]>10 and var[0]<=1500000:
                        self.hage10j.Fill(x,w)
                    elif var[1]<9 and var[0]<=1500000:
                        self.hase10j.Fill(x,w)

            self.loss_ge10j_self /= count_loss
            self.loss_self /= len(y_hat)
            self.loss_ge10j_self_list.append(self.loss_ge10j_self)
            self.loss_self_list.append(self.loss_self)
            self.ha.Add(self.hb)
            self.hage10j.Add(self.hbge10j)
            self.hase10j.Add(self.hbse10j)
            self.hb.Divide(self.hb,self.ha,1,1,"b")
            self.hbge10j.Divide(self.hbge10j,self.hage10j,1,1,"b")
            self.hbse10j.Divide(self.hbse10j,self.hase10j,1,1,"b")
            for i in range(n):
                self.hc.SetBinContent(i+1,self.hc.GetBinCenter(i+1))
            self.hb.SetName(self.hb.GetName()+str(epoch))
            self.hbge10j.SetName(self.hbge10j.GetName()+str(epoch))
            self.hbse10j.SetName(self.hbse10j.GetName()+str(epoch))
            self.hist_list.append(self.hb)

            cout=0.0
            coutge10j=0.0
            coutse10j=0.0
            for i in range(n):
                y = self.hb.GetBinContent(i+1)
                x = self.hc.GetBinContent(i+1)
                e = self.hb.GetBinError(i+1)
                yge10j = self.hbge10j.GetBinContent(i+1)
                ege10j = self.hbge10j.GetBinError(i+1)
                yse10j = self.hbse10j.GetBinContent(i+1)
                ese10j = self.hbse10j.GetBinError(i+1)
                if (y>0 and e>0 and y<1 and e<0.05 and abs(x-y)<0.1):
                    self.chi2_self+=((y-x)**2)/(e)
                    cout+=1.0
                if (yge10j>0 and ege10j>0 and yge10j<1 and abs(x-yge10j)<0.3):
                    self.chi2_ge10j_self+=((yge10j-x)**2)/(ege10j)
                    coutge10j+=1.0
                if (yse10j>0 and ese10j>0 and yse10j<1 and abs(x-yse10j)<0.3):
                    self.chi2_se10j_self+=((yse10j-x)**2)/(ese10j)
                    coutse10j+=1.0
 
            self.chi2_self/=(cout-1)
            self.chi2_ge10j_self/=(coutge10j-1)
            self.chi2_se10j_self/=(coutse10j-1)
            print('SELF CHI2 = ',self.chi2_self)
            print('SELF CHI2 G.E.10J = ',self.chi2_ge10j_self)
            print('SELF CHI2 S.E.10J = ',self.chi2_se10j_self)
            print('SELF LOSS = ',self.loss_self)
            print('SELF LOSS G.E.10J = ',self.loss_ge10j_self)
            self.chi2_self_list.append(self.chi2_self)
            self.chi2_ge10j_self_list.append(self.chi2_ge10j_self)
            self.chi2_se10j_self_list.append(self.chi2_se10j_self)
                
            self.chi2 = self.hb.Chi2Test(self.hc,"WW p CHI2/NDF")
            print('Linearity level by Chi2: ',self.chi2)
            self.chi2_list.append(self.chi2)
            self.save_plot(epoch)
        
        return 
    
    # Utility method
    def GetChi2(self):
        return self.chi2_list

    def GetChi2_self(self):
        return self.chi2_self_list,self.chi2_ge10j_self_list,self.chi2_se10j_self_list
    
    def GetLoss(self):
        return self.loss_self_list,self.loss_ge10j_self_list

    def GetHists(self):
        return self.hist_list
    
    def GetLrate(self):
        return self.lrate
