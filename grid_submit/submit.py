import os

f = open("../arguments.txt", "r")
line = f.readline()

hp_order=['hl','nd','lr','ep','bt']

while line:
    line=line.rstrip()
    hp_arg=''
    args=line.split(' ')
    for i in range(len(args)):
        hp_arg+=hp_order[i]+args[i]
    ex_cmd='prun --exec "source my_release_setup.sh;python3 hbsm_rw_NN_hpscan_grid.py %IN '+line+'" --inDS user.asonay:CERNPRODEFT4top --outDS user.asonay.hbsm.NNRW.'+hp_arg+' --outputs output.txt,out_model.h5,out_weight.h5,out_plot_test_epoch99.png,out_plot_train_epoch99.png --extFile my_release_setup.sh,mvasup.py,tfasistant.py,AnaStat.py,hbsm_rw_NN_hpscan_grid.py --maxAttempt=10 --noEmail'
    os.system(ex_cmd)
    line = f.readline()

f.close()
       
