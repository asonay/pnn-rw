


void create_bash(){
  ofstream outfile;
  outfile.open("arguments.txt");

  
  vector<float> nhl = {2,3,5,7,9};
  vector<float> nnode = {15,30,128,256};
  vector<float> lr = {0.0001,0.0003,0.0005,0.001,0.003,0.005,0.01};
  vector<float> ep = {100};
  vector<float> bt = {128,256,512,1024,2048};

  for (auto hl : nhl){
    for (auto node : nnode){
      for (auto lrate : lr){
	for (auto epoch : ep){
	  for (auto batch : bt){
	    outfile << hl << " "
		    << node << " "
		    << lrate << " "
		    << epoch << " "
		    << batch << endl;
	  }
	}
      }
    }
  }

}
