#!/usr/bin/env python3

# load and evaluate a saved model
import numpy as np
from keras.models import load_model
import ROOT as root
import mvasup
from array import array
import argparse
from glob import glob

import sys


def evaluate(ntuple_name,tree_name,model,norm,prefix_name):
    prefix = prefix_name
    nn_name = 'NN_score'+prefix
    nn_name = nn_name.replace('-','_')
    print ('\nName to be recorded:',nn_name)

    print ('NTUPLE: ',ntuple_name)
    print ('TREE  : ',tree_name)

    variable_names = norm.Read()

    f_read=root.TFile(ntuple_name)
    tree = f_read.Get(tree_name)
    
    tree_f=[]
    print ('Formulas are appending for :')
    for i in range(len(variable_names)):
        print (variable_names[i])
        tree_f.append(root.TTreeFormula('form_'+str(i),variable_names[i],tree))

    n_entries = tree.GetEntries()
    sample_size = len(tree_f)

    X=[]
    step=int(n_entries/20)

    print('Reading ntuple..')
    for i in range(n_entries):
        tree.GetEntry(i)
        x=[]
        for j,formula in enumerate(tree_f):
            formula.GetNdata()
            x.append(formula.EvalInstance())
        X.append(x)
        if (i%step)==0:
            print (i,'/',n_entries,'events has been done...')
    print('Finished..\n')
    f_read.Close()
    
    print ("Normalizing Inputs----------------------------------------")
    X = np.array(X,dtype=float)
    norm.Standardization(X)
    print ("End of Normalization--------------------------------------\n")

    Y=model.predict(X)
    print(Y)

    f_update = root.TFile(ntuple_name,'update')
    tree_update = f_update.Get(tree_name)
    n_entries = int(tree_update.GetEntries())

    score = array('f', [ 0. ] )
    score_branch = tree_update.Branch(nn_name,score,nn_name+'/F')

    print ('\nUpdating ntuple---------------------------------------')
    for i in range(n_entries):
        score[0]=Y[i]
        score_branch.Fill()

    tree_update.Write(tree_name)
    f_update.Close()
    print('Finished..--------------------------------------------')

def GetTreeNames(ntuple,tree_names):
    f0 = root.TFile(ntuple)

    trees=[]
    keys = f0.GetListOfKeys()
    if tree_names[0] == 'ALL':
        tree_name_pre = 'null_tree_name'
        for key in keys:
            if key.GetClassName() != 'TTree' : continue
            tree_tmp = key.ReadObj()
            tree_name = tree_tmp.GetName()
            noEx = True
            for name in tree_names:
                if name == tree_name: noEx=False
            if tree_name != tree_name_pre and noEx:
                trees.append(tree_name)
                print ('TREE NAME WILL BE FILLED! :  ',tree_name)
            tree_name_pre = tree_name
    else :
        trees=tree_names

    f0.Close()

    return trees

    
if __name__ == "__main__":

    parser = argparse.ArgumentParser("NN evaluate tool", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-n","--ntup", nargs='*', action="store", dest="ntuples", help="Ntuple file to be evaluated.", required=True)
    parser.add_argument("-t","--trees",action="store", dest="trees", help="ROOT Trees to be evaluated", required=True)
    parser.add_argument("-m","--model",action="store", dest="model", help="Keras model file (h5)", required=True)
    parser.add_argument("-s","--norm",action="store", dest="norm", help="Normalization file used in the training", required=True)
    parser.add_argument("-p","--prefix",action="store", dest="prefix", help="Prefix to be added variable name (NN_score+prefix)", required=True)
   
    args = parser.parse_args()

    ntuples = args.ntuples
    tree_names = args.trees.split(',')
    model_file = args.model
    norm_file = args.norm.replace('.csv','')
    prefix = args.prefix

    print ('NORM  : ',norm_file)
    norm=mvasup.Norm(norm_file,'')


    print ('MODEL : ',model_file)
    model = load_model(model_file,
                       custom_objects=None,
                       compile=False)

    for ntuple_name in ntuples:
        trees=GetTreeNames(ntuple_name,tree_names)
        for tree_name in trees:
            evaluate(ntuple_name,tree_name,model,norm,prefix)
    
    print ('\nDone.\n')
