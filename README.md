## Data Preparation  

To preparing training/test data, root ntuple can be converted CSV format by using the script,

```
root -l 'ntuple_to_csv_single.cpp("your_ntuple.root","tree_name","type","list_of_variable","output.csv")'
```

where the type is a string which can be specify as "s" (signal) or "b" (background) and list_of_variable is the variable list where you define your inputs. [Example list](https://gitlab.cern.ch/asonay/pnn-rw/-/blob/master/list/VariableList.txt)

## Training  


Your inputs and outputs has to be specified in first 15 to 21 line in "hbsm_rw_NN.py".
Then training procedure can be start by,

```
python3 hbsm_rw_NN.py
```

The monitoring of the training is under study.

Hyperparameter (HP) scan can be submit under the "grid_submit" directory.  

Please contact for further detail:
anil.sonay@cern.ch