
import numpy as np


def NjetSequence(x,y,w,njet_index):
    lst_7jex=[]
    lst_8jex=[]
    lst_9jex=[]
    lst_10jin=[]

    o1=[]
    o2=[]
    o3=[]
    
    for i in range(len(x)):
        if x[i][njet_index] == 7:
            lst_7jex.append(i)
        if x[i][njet_index] == 8:
            lst_8jex.append(i)
        if x[i][njet_index] == 9:
            lst_9jex.append(i)
        if x[i][njet_index] >= 10:
            lst_10jin.append(i)

    n_7jex=len(lst_7jex)
    n_8jex=len(lst_8jex)
    n_9jex=len(lst_9jex)
    n_10jin=len(lst_10jin)
    ntot=n_7jex+n_8jex+n_9jex+n_10jin

    if (n_7jex>0):
        ratio_7jex=int(float(ntot)/float(n_7jex))
    else:
        ratio_7jex=1
    if (n_8jex>0):
        ratio_8jex=int(float(ntot)/float(n_8jex))
    else:
        ratio_8jex=1
    if (n_9jex>0):
        ratio_9jex=int(float(ntot)/float(n_9jex))
    else:
        ratio_9jex=1
    if (n_10jin>0):
        ratio_10jin=int(float(ntot)/float(n_10jin))
    else:
        ratio_10jin=1
        
    c_7jex,c_8jex,c_9jex,c_10jin=0,0,0,0

    for cg in range(ntot):
        if (cg%ratio_7jex)==0 and c_7jex<n_7jex and len(lst_7jex)>0:
            o1.append(x[lst_7jex[c_7jex]])
            o2.append(y[lst_7jex[c_7jex]])
            o3.append(w[lst_7jex[c_7jex]])
            c_7jex+=1
        if (cg%ratio_8jex)==0 and c_8jex<n_8jex and len(lst_8jex)>0:
            o1.append(x[lst_8jex[c_8jex]])
            o2.append(y[lst_8jex[c_8jex]])
            o3.append(w[lst_8jex[c_8jex]])
            c_8jex+=1
        if (cg%ratio_9jex)==0 and c_9jex<n_9jex and len(lst_9jex)>0:
            o1.append(x[lst_9jex[c_9jex]])
            o2.append(y[lst_9jex[c_9jex]])
            o3.append(w[lst_9jex[c_9jex]])
            c_9jex+=1
        if (cg%ratio_10jin)==0 and c_10jin<n_10jin and len(lst_10jin)>0:
            o1.append(x[lst_10jin[c_10jin]])
            o2.append(y[lst_10jin[c_10jin]])
            o3.append(w[lst_10jin[c_10jin]])
            c_10jin+=1

    print('Ratio for different #Jets Regions (7jex,8jex,9jex,10jin):',ratio_7jex,ratio_8jex,ratio_9jex,ratio_10jin)
    print('Event numbers for different #Jets Regions (7jex,8jex,9jex,10jin):',n_7jex,n_8jex,n_9jex,n_10jin)
    print(c_7jex,'+',c_8jex,'+',c_9jex,'+',c_10jin,'=',ntot,(c_7jex+c_8jex+c_9jex+c_10jin)==ntot)
            
    return np.array(o1),np.array(o2),np.array(o3)



def MClassByNJets(X,Y,njet_index):

    o=[]

    for x,y in zip(X,Y):
        if int(x[njet_index]) == 7 and int(y) == 1:
            o.append([1,0,0,0])
        if int(x[njet_index]) == 7 and int(y) == 0:
            o.append([0,0,0,0])
        if int(x[njet_index]) == 8 and int(y) == 1:
            o.append([0,1,0,0])
        if int(x[njet_index]) == 8 and int(y) == 0:
            o.append([0,0,0,0])
        if int(x[njet_index]) == 9 and int(y) == 1:
            o.append([0,0,1,0])
        if int(x[njet_index]) == 9 and int(y) == 0:
            o.append([0,0,0,0])
        if int(x[njet_index]) >= 10 and int(y) == 1:
            o.append([0,0,0,1])
        if int(x[njet_index]) >= 10 and int(y) == 0:
            o.append([0,0,0,0])

    print (len(X),'=',len(o),len(X)==len(o))
    print ('New Array : ')
    print (Y)
    print (X[:,njet_index])
    print (np.array(o))
          
    return np.array(o)
