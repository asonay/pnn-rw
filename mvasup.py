import math
import numpy as np
import sys

class Norm:
    def __init__(self,name='Support',location='norm/'):
        self.__sig=0
        self.__avg=0
        self.__max_col=0
        self.__min_col=0
        self.__variable_names=0
        self.__read=False
        self.__file_name=location+name+'.csv'

    def Read(self):

        try:
            f = open(self.__file_name)
        except IOError:
            print(self.__file_name,'not accessible')
            sys.exit()
        header = f.readline()
        var = header.split(';')
        for i in range(len(var)):
            var[i] = var[i].replace('\n','')
        data=np.loadtxt(self.__file_name,delimiter=';',skiprows=1)
        self.__variable_names=np.array(var)
        self.__avg=data[0,:]
        self.__sig=data[1,:]
        self.__read=True
        return var
        
       
    def Standardization(self,X,var=0):
        variable_size=len(X[0])
        sample_size=len(X)
        print ('Detected Variable Size: ',variable_size,'and Sample Size: ', sample_size)
        if self.__read==False:
            self.__read=True
            sum_col = [0]*variable_size
            sum_square_col = [0]*variable_size
            for i in range(sample_size):
                for j in range(variable_size):
                    sum_col[j]+=X[i][j]
                    sum_square_col[j]+=X[i][j]*X[i][j]
            self.__avg = np.array([x / sample_size for x in sum_col])
            self.__sig = np.array([math.sqrt(x / sample_size) for x in sum_square_col])
            self.__variable_names = np.array(var)
            np.savetxt(self.__file_name,(self.__variable_names,self.__avg,self.__sig),delimiter=';', fmt="%s")


        for i in range(sample_size):
            for j in range(variable_size):
                X[i][j] = (X[i][j]-self.__avg[j])/self.__sig[j]
                
        for i in range(variable_size):
            print (self.__variable_names[i],self.__avg[i],self.__sig[i])
            
           
        return X

    def Normalization(self,X,var):
        variable_size=len(X[0])
        sample_size=len(X)
        print ('Detected Variable Size: ',variable_size,'and Sample Size: ', sample_size)
        self.__max_col = np.amax(X,axis=0)
        self.__min_col = np.amin(X,axis=0)
        self.__variable_names = np.array(var)

        for i in range(len(X)):
            for j in range(len(X[i])):
                X[i][j] = (X[i][j]-self.__min_col[j])/(self.__max_col[j]-self.__min_col[j])

            
        for i in range(len(var)):
            print (var[i],self.__max_col[i],self.__min_col[i])

        np.savetxt(self.__file_name,(self.__variable_names,self.__avg,self.__sig),delimiter=';', fmt="%s")
        
        return X

def CorrectNames(variable_names):
    for i in range(len(variable_names)):
        variable_names[i]=variable_names[i].replace('(','_')
        variable_names[i]=variable_names[i].replace(')','_')
        variable_names[i]=variable_names[i].replace('&','')
        variable_names[i]=variable_names[i].replace('$','')
        variable_names[i]=variable_names[i].replace('[','')
        variable_names[i]=variable_names[i].replace(']','')
        variable_names[i]=variable_names[i].replace('-','')
        variable_names[i]=variable_names[i].replace('+','')
        variable_names[i]=variable_names[i].replace('<','')
        variable_names[i]=variable_names[i].replace('/','_')
        variable_names[i]=variable_names[i].replace('>','')
        variable_names[i]=variable_names[i].replace('*','')
        variable_names[i]=variable_names[i].replace(',','_')
        variable_names[i]=variable_names[i].replace('=','_')

    return variable_names


def GetLOutput(var_data,var_mc,wmc):
    n=200
    var_conc = np.concatenate([var_data,var_mc])
    xmin = np.amin(var_conc)
    xmax = np.amax(var_conc)
    dx = (xmax-xmin)/n
    pd = [0]*(n+1)
    pmc = [0]*(n+1)
    for x in var_data:
        index = int(math.modf((x-xmin)/dx)[1])
        pd[index]+=1
    for x,w in zip(var_mc,wmc):
        index = int(math.modf((x-xmin)/dx)[1])
        pmc[index]+=w

    c = TCanvas('c')
    step = 0
    gx,gy = array('d'),array('d')
    for a,b in zip(pd,pmc):
        ratio = a/(b) if (b) !=0 else 0
        gx.append(xmin+step*dx)
        gy.append(ratio)
        step += 1
    box_pts=9
    box = np.ones(box_pts)/box_pts
    gyhat = np.convolve(np.array(gy), box, mode='same')
    gr = TGraph(n,gx,gy)
    grhat = TGraph(n,gx,gyhat)
    grhat.SetLineColor(2)
    gr.Draw('alp')
    grhat.Draw('lp same')
    c.SaveAs('test.png')

    x,y = np.frombuffer(gx), np.frombuffer(gy)
        
    return x,y

def SortAndShuffle(dataset,col):
    dataset=dataset[dataset[:,col].argsort()]
    noe=len(dataset)
    print (int((noe-5)/2))
    for i in range(int(((noe+2)/4)-1)):
        a=np.copy(dataset[2*i])
        b=np.copy(dataset[noe-2*i-2])
        dataset[2*i,:]=b
        dataset[noe-2*i-2,:]=a

    #for x in dataset:
     #   print(x)
    return dataset

def Shuffle(x1,x2,y1,y2,w1,w2):

    if len(x1) != len(y1) or len(x2) != len(y2) or len(x1) != len(w1):
        print ('X and Y size should be same !')
        sys.exit()
    
    n1 = len(x1)
    n2 = len(x2)
    o1=[]
    o2=[]
    o3=[]
    j=0
    if n1 >= n2 :
        ntot = n1
        ratio = int(float(n1)/float(n2))
        for i in range(n1):
            o1.append(x1[i])
            o2.append(y1[i])
            o3.append(w1[i])
            if (i % ratio) == 0 and j<n2:
                o1.append(x2[j])
                o2.append(y2[j])
                o3.append(w2[j])
                j+=1
    else :
        ntot = n2
        ratio = int(float(n2)/float(n1))
        for i in range(n2):
            o1.append(x2[i])
            o2.append(y2[i])
            o3.append(w2[i])
            if (i % ratio) == 0 and j<n1:
                o1.append(x1[j])
                o2.append(y1[j])
                o3.append(w1[j])
                j+=1

    print ('Shuffled data size: ',n1,'+',n2,'=',len(o2),(n1+n2)==len(o2))
    return np.array(o1),np.array(o2),np.array(o3)


def kfold_split(ntot,nfold):
    split_sample = []
    for i in range(nfold):
        lst=[]
        for j in range(ntot):
            if (j%nfold)==i:
                lst.append(j)
    split_sample.append(lst)

    return split_sample

def train_test_split(y,percentage):
    lst_test=[]
    lst_train=[]
    if (percentage!=0):
        idn = int(100/percentage)
    else:
        idn = int(0)
    cpos=0
    cneg=0
    ntot = len(y)
    for j in range(ntot):
        if idn!=0:
            if y[j]==1:
                if ((cpos%idn)==0):
                    lst_test.append(j)
                else :
                    lst_train.append(j)
                cpos+=1
            if y[j]==0:
                if ((cneg%idn)==0):
                    lst_test.append(j)
                else :
                    lst_train.append(j)
                cneg+=1
        else:
            lst_train.append(j)

    print('Splitting Percentage:',len(lst_test)/ntot,'%%  | ',len(lst_test),'+',len(lst_train),'=',len(lst_test)+len(lst_train),ntot==len(lst_test)+len(lst_train))
            
    return lst_train,lst_test

from ROOT import TGraph,TH1D,TH2D,TCanvas
from array import array

def GetGraph(data,nstep):
    if len(data)>0:
        gn = len(data)
        gx, gy = array( 'd' ), array( 'd' )
        for i in range(gn):
            gx.append(i*nstep+1)
            gy.append(data[i])
        gr=TGraph( gn, gx, gy )
        return gr
    else:
        print('Data size must be more than 0')
        return TGraph(0)


def GetSeparations(x,y,names):

    hs=[]
    hb=[]
    nbins=100
    xmin=np.amin(x,axis=0)
    xmax=np.amax(x,axis=0)
    dx=(xmax-xmin) / nbins
    for i,name in enumerate(names):
        hs.append(TH1D('hSignal_'+name,name,nbins,xmin[i]*0.99,xmax[i]*1.01))
        hb.append(TH1D('hBackground_'+name,name,nbins,xmin[i]*0.99,xmax[i]*1.01))
    
    for i,variables in enumerate(x):
        for j,var in enumerate(variables):
            if y[i] == 1:
                hs[j].Fill(var)
            else:
                hb[j].Fill(var)

    sep=[]
    print('Variables sepearations==================')
    for i,name in enumerate(names):
        s=0
        b=0
        tot=0
        for j in range(nbins):
            s=hs[i].GetBinContent(j+1)/(hs[i].GetSumOfWeights()*dx[i])
            b=hb[i].GetBinContent(j+1)/(hb[i].GetSumOfWeights()*dx[i])
            if s+b>0:
                tot+=(0.5*(s-b)*(s-b)/(s+b))*dx[i]
        sep.append(tot)
        print (name,':',tot)
        
    print('========================================')
    return sep, hs, hb

def CorrelationMatrix(x,names,hname):
            
    corr=np.corrcoef(x.T)
    nvar=len(names)
    h=TH2D("hCorrMat"+hname,"",nvar+2,0,nvar+2,nvar+2,0,nvar+2);

    print('Correlation matrix======================')
    for i in range(nvar):
        h.GetXaxis().SetBinLabel(i+2,names[i])
        h.GetYaxis().SetBinLabel(i+2,names[i])
        print(names[i],corr[i])
        for j in range(nvar):
            h.SetBinContent(i+2,j+2,corr[i][j])
    print('========================================')

    c=TCanvas('CorrMat')

    h.Draw('colz')
    h.Draw('text same')

    c.SaveAs('plots/correlation_matrix'+hname+'.png')

    return corr, h

def GetImportance(corr,sep,names):

    tot_corr=[]

    for corri in corr:
        mul_corr=1.0
        for corrj in corri:
            if corrj!=1:
                mul_corr*=(1-corrj)
        tot_corr.append(mul_corr)

    imp=[]
    for s,c in zip(sep,tot_corr):
        imp.append(s*c)

    imp=imp/sum(imp)
    
    print('Importance======================')
    print(names)
    print(imp)
    print('================================')
    
    return imp

def SelectVariables(selections,names):

    o=[]
    nov=0
    
    for i,name in enumerate(names):
        if name == 'weights':
            nov=i-1
            break

    if nov == 0:
        print ('Error :: Weights label can not be found, your file is not designed well to use this function.')
        sys.exit()

    for selection in selections:
        is_name_find=False
        for i,name in enumerate(names):
            if selection == name:
                o.append(i)
                is_name_find=True
        if is_name_find==False:
            print ('Error :: Desired name can not be found for :',selection)
            sys.exit()
    
    return o,nov

def WriteToFile(x,f):
    for e in x:
        f.write('%s ' % e)
    f.write('\n')
