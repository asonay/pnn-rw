#!/usr/bin/env python3

import numpy as np
from keras.models import Sequential
from keras.optimizers import SGD,Adam,RMSprop
from keras.layers import Dense, Dropout, LeakyReLU, AlphaDropout, BatchNormalization
import mvasup
import specsup
import math
import tfasistant
import sys
from ROOT import TFile, TTree, TGraph, TCanvas
from array import array
from tensorflow.python.keras.callbacks import Callback
from keras.callbacks import LearningRateScheduler

prefix = '_4D_5Ltanh_20ep_node30_bt1000_kin_mclass'
file_prefix = '_2b'#'_njnb_blind'#'_2b'#'_7jin2bex-7jex3bin0J'#
prefix += file_prefix
nepoch=21
#input
filenames = ['/eos/user/a/asonay/MVA_Data/data/ttbar_PhPy8_AFII_AllFilt'+file_prefix+'.csv',
             '/eos/user/a/asonay/MVA_Data/data/ttbar_PhHerwig_AFII_AllFilt'+file_prefix+'.csv',
             '/eos/user/a/asonay/MVA_Data/data/data'+file_prefix+'.csv',
             '/eos/user/a/asonay/MVA_Data/data/non-ttbar'+file_prefix+'.csv',
             ]
#output
output_model_name = '/eos/user/a/asonay/MVA_Data/weights/RW_model_1l'+prefix+'.h5'
output_weights_name = '/eos/user/a/asonay/MVA_Data/weights/RW_weights_1l'+prefix+'.h5'
output_file_name = '/eos/user/a/asonay/MVA_Data/RW_output_1l'+prefix+'.root'

#Selection of variables to be trained.
#variable_names = ['Alt$(jet_pcb_MV2c10_btag_ordered[2],0)','nJets','Alt$(jet_pcb_MV2c10_btag_ordered[3],0)','Alt$(jet_pcb_MV2c10_btag_ordered[4],0)']
variable_names = ['nRCJetsM100','nJets','jet_pt[0]','jet_pt[1]','jet_pt[2]','jet_pt[3]','jet_pt[4]','jet_pt[5]','jet_pt[6]','Alt$(jet_pt[7],0)','Alt$(jet_pt[8],0)','Sum$(jet_pt*(Iteration$>=9))','Alt$(el_pt[0],0)+Alt$(mu_pt[0],0)','met_met']

dataset=[]
number_of_variable=[]
selected_variable=0
for filename in filenames:
    print ("\nReading file: ",filename)
    try:
        f = open(filename)
    except IOError:
        print(filename,'not accessible')
        sys.exit()
    header = f.readline()
    header_names = np.array(header.split(';'))
    selected_variable,nov = mvasup.SelectVariables(variable_names,header_names)
    dataset.append(np.loadtxt(filename, delimiter=';', skiprows=1, converters={nov+2: lambda x:int(x=='s'.encode('utf-8')) }))
    number_of_variable.append(nov)
print ("Completed..\n")

variable_size = len(selected_variable)
print ('    \nVariable Size : ',variable_size)

X_data = dataset[2][:,selected_variable]
X_nonttbar = dataset[3][:,selected_variable]
X_mc = dataset[0][:,selected_variable]
X_mc_alt = dataset[1][:,selected_variable]
data_size,nonttbar_size,mc_size,mc_alt_size = len(X_data),len(X_nonttbar),len(X_mc),len(X_mc_alt)
W_data = np.ones(data_size)
W_nonttbar = dataset[3][:,number_of_variable[3]+1] * -1
W_mc = dataset[0][:,number_of_variable[0]+1]
W_mc_alt = dataset[1][:,number_of_variable[1]+1]

Wnmc = sum(W_mc)
Wnmc_alt = sum(W_mc_alt)
Wnnonttbar = sum(W_nonttbar)

print ('\nData Size:',data_size,'Non-ttbar Size:',nonttbar_size,'MC Size:',mc_size,'MC Alt Size:',mc_alt_size,'Weighted MC:',Wnmc,'Weighted Alt MC:',Wnmc_alt,'Weighted non-ttbar:',Wnnonttbar)


Y_data,Y_nonttbar,Y_mc,Y_mc_alt = np.array([[0,0]]*data_size), np.array([[0,0]]*nonttbar_size), np.array([[1,0]]*mc_size), np.array([[0,1]]*mc_alt_size)

noo=Y_data.shape[1]
print('\nNumber of Outputs:',noo)

X_data,Y_data,W_data=specsup.NjetSequence(X_data,Y_data,W_data,1)
X_nonttbar,Y_nonttbar,W_nonttbar=specsup.NjetSequence(X_nonttbar,Y_nonttbar,W_nonttbar,1)
X_mc,Y_mc,W_mc=specsup.NjetSequence(X_mc,Y_mc,W_mc,1)
X_mc_alt,Y_mc_alt,W_mc_alt=specsup.NjetSequence(X_mc_alt,Y_mc_alt,W_mc_alt,1)

X_ttbardata,Y_ttbardata,W_ttbardata = mvasup.Shuffle(X_data,X_nonttbar,Y_data,Y_nonttbar,W_data,W_nonttbar)
X_mcall,Y_mcall,W_mcall = mvasup.Shuffle(X_mc,X_mc_alt,Y_mc,Y_mc_alt,W_mc,W_mc_alt)
X,Y,W = mvasup.Shuffle(X_mcall,X_ttbardata,Y_mcall,Y_ttbardata,W_mcall,W_ttbardata)
#Y=Y.reshape((len(Y), 1))
#W=W.reshape((len(W), 1))

print ('\nSplitting samples..')
lst_train,lst_test=mvasup.train_test_split(Y,0)
print ('\nSplitting done.\n')

print ("Normalizing Inputs----------------------------------------")
norm=mvasup.Norm('strd_rw'+prefix,'norm/')
X_train=norm.Standardization(np.copy(X),variable_names)
print ("End of Normalization--------------------------------------\n")
#seps,hs,hb=mvasup.GetSeparations(X,Y,variable_names)
#corr,hcorr=mvasup.CorrelationMatrix(X_train,variable_names,'standard')
#imp=mvasup.GetImportance(corr,seps,variable_names)


print(X)
print(X_train)
print(Y)
print(W)


# define the keras model
# P.S.: Due to dropout directly effect Loss function, it is also effect posterior probability.
# There is a paper which shows dropout can be used for uncertainity calculation : https://arxiv.org/pdf/1512.05287.pdf
model = Sequential()
model.add(Dense(30, input_dim=variable_size, activation='relu', kernel_initializer='he_uniform'))
#model.add(Dropout(0.2))
model.add(Dense(30, activation='relu', kernel_initializer='he_uniform'))
#model.add(Dropout(0.3))
model.add(Dense(30, activation='relu', kernel_initializer='he_uniform'))
#model.add(Dropout(0.3))
model.add(Dense(30, activation='relu', kernel_initializer='he_uniform'))
#model.add(Dropout(0.3))
model.add(Dense(30, activation='tanh'))
#model.add(Dropout(0.3))
model.add(Dense(noo, activation='softmax'))
# compile the keras model
opt = SGD(lr=0.0036, momentum=0.9)
lrate = LearningRateScheduler(tfasistant.step_decay)
#opt = RMSprop(learning_rate=0.0001, rho=0.9)
#opt = Adam(learning_rate=1e-03, beta_1=0.9, beta_2=0.999, amsgrad=False)

model.compile(loss='mean_squared_error', optimizer=opt, metrics=['accuracy'])
print (model.summary())
# fit the keras model on the dataset
#Metric need to be optimized
nstep=20
#callback_train = tfasistant.Callbacks('train',X_train[lst_train,:],X[lst_train,:],Y[lst_train],W[lst_train],nstep)
#callback_test = tfasistant.Callbacks('test',X_train[lst_test,:],X[lst_test,:],Y[lst_test],W[lst_test],nstep)
model.fit(X_train[lst_train,:], Y[lst_train], sample_weight = W[lst_train], epochs=nepoch, batch_size=1000)#, callbacks=[callback_train,callback_test])#,lrate])
# evaluate the keras model
model.save(output_model_name)
model.save_weights(output_weights_name)

#gr_chi2_basic_train=mvasup.GetGraph(callback_train.GetChi2(),nstep)
#gr_chi2_basic_test=mvasup.GetGraph(callback_test.GetChi2(),nstep)
#gr_chi2_self_train=mvasup.GetGraph(callback_train.GetChi2_self()[0],nstep)
#gr_chi2_self_test=mvasup.GetGraph(callback_test.GetChi2_self()[0],nstep)
#gr_chi2_self_train_ge10j=mvasup.GetGraph(callback_train.GetChi2_self()[1],nstep)
#gr_chi2_self_test_ge10j=mvasup.GetGraph(callback_test.GetChi2_self()[1],nstep)
#gr_chi2_self_train_se10j=mvasup.GetGraph(callback_train.GetChi2_self()[2],nstep)
#gr_chi2_self_test_se10j=mvasup.GetGraph(callback_test.GetChi2_self()[2],nstep)
#grl=mvasup.GetGraph(callback_train.GetLrate(),nstep)

Ypred_train = model.predict(X_train[lst_train,:])
Ypred_test = model.predict(X_train[lst_test,:])
Ypred_all = model.predict(X_train)

print ("Storing into NTuple---------------------------------------\n")

class_id = array('i', [ 0 ]*noo )
variables = variable_size*[0]
variables_tvar = variable_size*[0]
for i in range(variable_size):
    variables[i] = array('f', [ 0. ] )
for i in range(variable_size):
    variables_tvar[i] = array('f', [ 0. ] )
weight = array('f', [ 0. ] )
score = array('f', [ 0. ]*noo )

Xo_train,Yo_train,Wo_train=X[lst_train,:],Y[lst_train,:],W[lst_train]
Xo_test,Yo_test,Wo_test=X[lst_test,:],Y[lst_test,:],W[lst_test]

print (Yo_train,Xo_train.shape,Yo_train.shape)
print (Yo_test,Xo_test.shape,Yo_test.shape)

f = TFile(output_file_name,'recreate')
t1 = TTree('tr','Tree for Training Output')
t2 = TTree('tr_test','Tree for Test Output')
t3 = TTree('tr_all','Tree for Test Output')

variable_names=mvasup.CorrectNames(variable_names)

t1.Branch('class_id',class_id,'class_id['+str(noo)+']/I')
for i in range(variable_size):
    t1.Branch(variable_names[i],variables[i],variable_names[i]+'/F')
t1.Branch('weight',weight,'weight/F')
t1.Branch('score',score,'score['+str(noo)+']/F')

t2.Branch('class_id',class_id,'class_id['+str(noo)+']/I')
for i in range(variable_size):
    t2.Branch(variable_names[i],variables[i],variable_names[i]+'/F')
t2.Branch('weight',weight,'weight/F')
t2.Branch('score',score,'score['+str(noo)+']/F')

t3.Branch('class_id',class_id,'class_id['+str(noo)+']/I')
for i in range(variable_size):
    t3.Branch(variable_names[i],variables[i],variable_names[i]+'/F')
for i in range(variable_size):
    t3.Branch(variable_names[i]+'_tvar',variables_tvar[i],variable_names[i]+'_tvar/F')
t3.Branch('weight',weight,'weight/F')
t3.Branch('score',score,'score['+str(noo)+']/F')

for i in range(len(Xo_train)):
    for j in range(noo):
        class_id[j] = int(Yo_train[i][j])
    for j in range(variable_size):
        variables[j][0] = Xo_train[i][j]
    weight[0] = Wo_train[i]
    for j in range(noo):
        score[j] = Ypred_train[i][j]
    t1.Fill()
t1.Write()

for i in range(len(Xo_test)):
    for j in range(noo):
        class_id[j] = int(Yo_test[i][j])
    for j in range(variable_size):
        variables[j][0] = Xo_test[i][j]
    weight[0] = Wo_test[i]
    for j in range(noo):
        score[j] = Ypred_test[i][j]
    t2.Fill()
t2.Write()

for i in range(len(X)):
    for j in range(noo):
        class_id[j] = int(Y[i][j])
    for j in range(variable_size):
        variables[j][0] = X[i][j]
    for j in range(variable_size):
        variables_tvar[j][0] = X_train[i][j]
    weight[0] = W[i]
    for j in range(noo):
        score[j] = Ypred_all[i][j]
    t3.Fill()
t3.Write()

#gr_chi2_basic_train.Write("GrTrain")
#gr_chi2_basic_train.Write("GrTest")
#gr_chi2_self_train.Write("GrTrain_self")
#gr_chi2_self_test.Write("GrTest_self")
#gr_chi2_self_train_ge10j.Write("GrTrain_self_ge10j")
#gr_chi2_self_test_ge10j.Write("GrTest_self_ge10j")
#gr_chi2_self_train_se10j.Write("GrTrain_self_se10j")
#gr_chi2_self_test_se10j.Write("GrTest_self_se10j")
#grl.Write("GrLrate")

#hists_train = callback_train.GetHists()
#hists_test = callback_test.GetHists()

#for x in hists_train:
#    x.Write(x.GetName())
#for x in hists_test:
#    x.Write(x.GetName())
#for x in hs:
#    x.Write()
#for x in hb:
#    x.Write()
#hcorr.Write()

f.Write()
f.Close()


print ("Storing the data has been done..------------------------")
